import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'projeto-angular-tc2';

  component : string = "register";
  inputName : string;
  inputAge : number;
  inputImg : string;
  registriesArray: any[] = [];
  viewLog : any = [];

  constructor(private service: AppService) {};

  setInput(): any {
    const check = this.registriesArray.some(pessoa => pessoa.nome === this.inputName)
    if (this.inputName && this.inputAge && !check) {
      console.log(check)
      alert("Cadastro reslizado com sucesso!")
      this.service.setNewRegistries(this.inputName, this.inputAge, this.inputImg);
    }
    else {
      alert("Preencha ou corrija os campos!")
    }
  }

  viewLogs(elem : any): any {
    this.component = "form";
    this.viewLog = {
      nome: elem.nome,
      idade: elem.idade,
      img: this.check(elem.img)
    }
  }

  check(elem : any) : any {
    if (elem) return elem
    else return "https://picsum.photos/200"
  }

  handleClickRegister(): any {
    this.component = "register";
  }

  handleClickForm(): any {
    this.component = "form";
  }

  ngOnInit() {
    this.registriesArray = this.service.registries;
  }

}
