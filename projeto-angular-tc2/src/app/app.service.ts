import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  registries : any = [
    {nome: 'Exemplo', idade: 21, img: 'https://picsum.photos/200'},
  ];

  setNewRegistries(name: string, age: number, image: string) : any {
    const object = {nome: name, idade: age, img: image};
    this.registries.push(object);
  }

  constructor() { }
}
